# PARSER

The tool uses javascript to test a given expression and check if it is valid or not. If it is a valid expression it returns the value of the expression. It also uses Qunit testing for writing testcases in Javascript.


## A brief explanation

This tool can be used to parse statments written in human readable format.
For example :

```
run("do( define(count, 1),",
    " while(<(count, 5),",
    "do( define(count, +(count, 1)))),",
    "print(count))"

```

This returns the output as 5.

## run() Function

This function takes the statements as parameters and execute them. It identifies the statements separated by "\n". Each statement is parsed by the parse function.

